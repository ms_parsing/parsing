"use strict";
const amqp = require("amqplib/callback_api");

amqp.connect("amqp://localhost", function(error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function(error1, channel) {
    if (error1) {
      throw error1;
    }
    const queue = "send";
    const msg = "Send Parsing";

    channel.assertQueue(queue, {
      durable: false
    });

    let iteration = 0;
    
    setInterval(()=>{
        iteration++;

        if(iteration === 10){
            connection.close();
            process.exit(0)
        }

        channel.sendToQueue(queue, Buffer.from(`${msg}-${iteration}`));
        console.log(" [x] Sent %s", msg, iteration);
    })

    

  });
});

