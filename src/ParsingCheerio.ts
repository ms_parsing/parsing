"use strict";
import { Parsing } from "./interface/Parsing";
const cheerio = require("cheerio");


export class ParsingCheerio implements Parsing {
    
    constructor(){}
    html: string = "";

    checkTitle(title: Array<string>): boolean {
        const blackList = [
            "жена",
            "Кейру",
            "девшука",
            "разводе",
            "измена",
            "Путин"
        ],
        len = blackList.length;

        for(let i = 0; i < len; i++) {
            if( title.indexOf(blackList[i]) > -1 ) {
                return false;
            }
        }

        return true
    }


    getData(html: string){
        const listLink: Array<Object> = [],
            parsing = cheerio.load(html);

        parsing("a._2XCUXj").each((idx: number, elem: object) => {

            const title = parsing(elem).text();

            if (this.checkTitle(title)) {
                listLink.push({
                    title: title,
                    href: parsing(elem).attr("href"),
                });
            }
        });

        return listLink;

    }

}