"use strict";
import { GetHtml } from "./GetHtml";
import { ParsingCheerio } from "./ParsingCheerio";


(async ()=>{
    const html = new GetHtml();

    const links = await html.html(new ParsingCheerio());
    
    console.log(links);
})()


