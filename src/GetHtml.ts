"use strict";
const https = require("https");
const fs = require("fs");
import { Parsing } from "./interface/Parsing"

export class GetHtml {
    address: string;
    fileName: string
    constructor(){
        this.address = "https://sport24.ru/football";
        this.fileName = this.getName();
    }

    getName(): string{
        const time = new Date(),
            name = `${time.getHours()}_${time.getMinutes()}_${time.getDate()}_${time.getMonth()}_${time.getFullYear()}`

        return `${process.cwd()}/files/html_${name}.html`;
    }


    html(parsing: Parsing): Promise<void> {
        return new Promise<void>((resolve, reject) => {
            https.get(this.address, (res:any) => {

                console.log(typeof res);

                let data = "";

                if (res.statusCode !== 200) {
                    res.resume();
                    return;
                }

                res.on("data", (chunk: any) => {
                    data += chunk;
                });


                res.on("end", (e: any) => {
                    fs.writeFileSync(
                        this.fileName,
                        data
                    );
                    const listLink = parsing.getData(data);

                    resolve(listLink);
                })

            }).on("error", (e:any) => {
                reject(e);
            });
        });


    }


}