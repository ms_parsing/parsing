import { readFileSync } from 'fs';

import { ParsingCheerio } from '../src/ParsingCheerio';

const file = readFileSync(process.cwd()+'/tests/mock/html_22_56_20_5_2021.html', {encoding:'utf-8', flag: 'r'})

const parsing = new ParsingCheerio();

const result = parsing.getData(file);

// Проверим пришёл ли массив
test('Check type response parsing', ()=>{
    expect(result instanceof Array).toBe(true)
});